import os
import sys
import subprocess
import tarfile
from optparse import OptionParser


def exec_cmd(cmd):
    print cmd
    os.system(cmd)


def main():
    # usage description
    usage = "Usage: python %prog -i InputDir -o OutputDir"

    # input parameters
    parser = OptionParser(usage=usage)

    parser.add_option("-i", "--input_dir", dest="input_dir",
                      help="Inut directory",
                      metavar="INPUT")

    parser.add_option("-o", "--output_dir", dest="output_dir",
                      help="Output directory",
                      metavar="OUTPUT")

    parser.add_option("-m", "--modules", dest="modules",
                      help="Comma-separated list of modules or module ranges (just number without the leading M)",
                      metavar="MODULES")

    (options, args) = parser.parse_args()

    # make sure all necessary input parameters are provided
    if not (options.input_dir and options.output_dir):
        print 'Mandatory input arguments missing'
        print ''
        parser.print_help()
        sys.exit(1)

    input_dir = options.input_dir
    if not input_dir.startswith('/'):
        input_dir = os.path.join( os.path.abspath('.'), input_dir )

    output_dir = options.output_dir
    if not output_dir.startswith('/'):
        output_dir = os.path.join( os.path.abspath('.'), output_dir )

    content = os.listdir(input_dir)
    content.sort()

    os.chdir(output_dir)

    for f in content:
        fullpath = os.path.join(input_dir,f)

        if not os.path.isfile(fullpath): continue

        extensions = ('.tar')
        if not f.lower().endswith(extensions): continue

        module = f.split('_')[0][1:]
        skip = True

        if options.modules:
            modules = options.modules.split(',')
            for m in modules:
                if '-' in m:
                    m_range = m.split('-')
                    m_low, m_high = [m_range[i] for i in (0, 1)]
                    if m_high != '':
                        if int(module) >= int(m_low) and int(module) <= int(m_high):
                            skip = False
                            break
                    else:
                        if int(module) >= int(m_low):
                            skip = False
                            break
                else:
                    if module == m:
                        skip = False
                        break
        else:
            skip = False

        if skip:
            continue

        print ''
        print f

        cmd = 'cp %s %s' % (fullpath, output_dir)
        exec_cmd(cmd)

        cmd = 'tar xfz %s' % f
        exec_cmd(cmd)

        dir_name = os.path.splitext(f)[0]

        cmd = 'rm %s' % f
        exec_cmd(cmd)

        cmd = 'grep "253 ctrlreg    13" */*/dacParameters40_C*.dat'
        print cmd
        output = ''
        returncode = 0
        try:
            output = subprocess.check_output(cmd, shell=True)
        except subprocess.CalledProcessError as e:
            returncode = e.returncode

        if returncode == 0 and output != '':
            # make the needed replacements
            #cmd = "sed -i 's/253 ctrlreg    13/253 ctrlreg    9 /g' */*/dacParameters40_C*.dat"
            # make the needed replacements but also preserve the original time stamps
            cmd = """find ./ -type f -name 'dacParameters40_C*.dat' -exec bash -c 't=$(stat -c %y "$0"); sed -i -e "s/253 ctrlreg    13/253 ctrlreg    9 /g" "$0"; touch -d "$t" "$0"' {} \;"""
            exec_cmd(cmd)

            print 'Making tar file'
            tar = tarfile.open(f, "w:gz")
            tar.add(dir_name)
            tar.close()

            cmd = 'touch -r %s %s' % (fullpath, os.path.join(output_dir, f))
            exec_cmd(cmd)

            cmd = 'rm -rf %s' % dir_name
            exec_cmd(cmd)
        else:
            cmd = 'rm -rf %s' % dir_name
            exec_cmd(cmd)


if __name__ == '__main__':
    main()
