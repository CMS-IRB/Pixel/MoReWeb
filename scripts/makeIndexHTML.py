import os
import sys
from optparse import OptionParser


def main():
    # usage description
    usage = "Usage: python %prog -p PATH"

    # input parameters
    parser = OptionParser(usage=usage)

    parser.add_option("-p", "--path", dest="path",
                      help="Path to folders that need to be indexed",
                      metavar="PATH")

    parser.add_option("--parent", dest="parent", action='store_true',
                      help="Also process the parent folder (This parameter is optional)",
                      default=False)

    (options, args) = parser.parse_args()

    # make sure all necessary input parameters are provided
    if not options.path:
        print 'Mandatory input argument missing'
        print ''
        parser.print_help()
        sys.exit(1)

    pwd = os.path.abspath('.')

    content = os.listdir(options.path)

    if options.parent:
        fullpath = os.path.join(pwd,options.path)
        os.chdir(fullpath)
        cmd = "tree -Hshi '.' -T '' -I index.html -L 1 --noreport --charset utf-8 > index.html"
        print 'Processing folder ' + fullpath
        os.system(cmd)

    for f in content:
        fullpath = os.path.join(pwd,options.path,f)

        if os.path.isfile(fullpath): continue

        os.chdir(fullpath)
        cmd = "tree -Hshi '.' -T '' -I index.html -L 1 --noreport --charset utf-8 > index.html"
        print 'Processing subfolder ' + fullpath
        os.system(cmd)


if __name__ == '__main__':
    main()
