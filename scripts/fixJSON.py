import os
import sys
from optparse import OptionParser
from makeProductionDB import query_yes_no


def main():
    # usage description
    usage = "Usage: python %prog -i InputDir -o OutputDir"

    # input parameters
    parser = OptionParser(usage=usage)

    parser.add_option("-i", "--input_dir", dest="input_dir",
                      help="Inut directory",
                      metavar="INPUT")

    parser.add_option("-o", "--output_dir", dest="output_dir",
                      help="Output directory",
                      metavar="OUTPUT")

    (options, args) = parser.parse_args()

    # make sure all necessary input parameters are provided
    if not (options.input_dir and options.output_dir):
        print 'Mandatory input arguments missing'
        print ''
        parser.print_help()
        sys.exit(1)

    input_dir = options.input_dir
    if not input_dir.startswith('/'):
        input_dir = os.path.join( os.path.abspath('.'), input_dir )

    output_dir = options.output_dir
    if not output_dir.startswith('/'):
        output_dir = os.path.join( os.path.abspath('.'), output_dir )

    content = os.listdir(input_dir)
    content.sort()

    os.chdir(output_dir)

    for f in content:
        input_path = os.path.join(input_dir,f)
        output_path = os.path.join(output_dir,f)

        if not os.path.isfile(input_path): continue

        extensions = ('.json')
        if not f.lower().endswith(extensions): continue

        input_json = []
        with open(input_path, 'rb') as in_json:
            input_json = in_json.readlines()

        if os.path.exists(output_path):
            question = "Output JSON file '" + output_path + "' already exists. Overwrite?"
            if query_yes_no(question, default="no"):
                pass
            else:
                continue

        out_json = open(output_path,'w')
        out_json.write('{\n')

        pruned_input_json = []
        for line in input_json:
            # skip empty lines
            if line.strip() == '':
                continue
            pruned_input_json.append(line)

        for counter, line in enumerate(pruned_input_json):
            key = line.split()[1].rstrip(',')
            new_line = '%s: %s' % (key, line.replace('}', '}' if (counter==len(pruned_input_json)-1) else '},').rstrip('\n'))
            out_json.write(new_line)

        out_json.write('}\n')
        out_json.close()


if __name__ == '__main__':
    main()
