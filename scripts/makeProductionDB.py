import os
import sys
import pickle
from optparse import OptionParser
# import `load_workbook` module from `openpyxl`
from openpyxl import load_workbook


# class holding module info
class ModuleInfo():
    def __init__(self):
        self.attributes = {'Chip0'  : None,
                           'Chip1'  : None,
                           'Chip2'  : None,
                           'Chip3'  : None,
                           'Chip4'  : None,
                           'Chip5'  : None,
                           'Chip6'  : None,
                           'Chip7'  : None,
                           'Chip8'  : None,
                           'Chip9'  : None,
                           'Chip10' : None,
                           'Chip11' : None,
                           'Chip12' : None,
                           'Chip13' : None,
                           'Chip14' : None,
                           'Chip15' : None,
                           'Sensor' : None,
                           'HDI'    : None
                           }


def query_yes_no(question, default="yes"):
    """Ask a yes/no question via raw_input() and return their answer.

    "question" is a string that is presented to the user.
    "default" is the presumed answer if the user just hits <Enter>.
        It must be "yes" (the default), "no" or None (meaning
        an answer is required of the user).

    The "answer" return value is True for "yes" or False for "no".
    """
    valid = {"yes": True, "y": True,
             "no": False, "n": False}
    if default is None:
        prompt = " [y/n] "
    elif default == "yes":
        prompt = " [Y/n] "
    elif default == "no":
        prompt = " [y/N] "
    else:
        raise ValueError("invalid default answer: '%s'" % default)

    while True:
        sys.stdout.write(question + prompt)
        choice = raw_input().lower()
        if default is not None and choice == '':
            return valid[default]
        elif choice in valid:
            return valid[choice]
        else:
            sys.stdout.write("Please respond with 'yes' or 'no' "
                             "(or 'y' or 'n').\n")


def write_db(db, outfile):
    # dump modules dictionary into a pickle file
    print('Saving module assembly info into a file...')
    with open(outfile, 'wb') as fpkl:
        pickle.dump(db, fpkl)
    print("File '%s' saved"%outfile)


def main():
    # usage description
    usage = "Usage: python %prog [options] \nExample: python %prog --xls Module_assembly_info.xlsx"

    # input parameters
    parser = OptionParser(usage=usage)

    parser.add_option("--xls", dest="xls",
                      help="Excel spreadsheet containing module assembly information",
                      metavar="XLS")

    parser.add_option("--sheet", dest="sheet",
                      help="Data sheet from the Excel spreadsheet to be used (This parameter is optional and set to 'Assembly datasheet' by default)",
                      default='Assembly datasheet',
                      metavar="SHEET")

    parser.add_option("--inDB", dest="inDB",
                      help="Input file storing previously collected module assembly information (This parameter is optional)",
                      metavar="INDB")

    parser.add_option("--outDB", dest="outDB",
                      help="Output file storing module assembly information (This parameter is optional and set to 'ModuleInfoDB.pkl' by default)",
                      default="ModuleInfoDB.pkl",
                      metavar="OUTDB")

    (options, args) = parser.parse_args()

    # make sure all necessary input parameters are provided
    if not options.xls:
        print 'Mandatory input argument missing'
        print ''
        parser.print_help()
        sys.exit(1)

    # load the Excel spreadsheet
    wb = load_workbook(options.xls)

    # get sheet names
    #print(wb.get_sheet_names())

    # get a sheet by name
    sheet = wb.get_sheet_by_name(options.sheet)

    # print the sheet title
    #print(sheet.title)

    # print the maximum number of columns in the sheet
    #print(sheet.max_column)

    # print the maximum number of rows in the sheet
    #print(sheet.max_row)

    # dictionary holding module info
    modulesInfo = {}

    # load previously collected module assembly info
    if options.inDB:
        with open(options.inDB, 'rb') as fpkl:
            modulesInfo  = pickle.load(fpkl)

    module = ''
    newModules = []
    newHDI = []

    # read in module assembly info
    for row in range(2, sheet.max_row+1):
        #print(row, sheet.cell(row=row, column=1).value)
        if sheet.cell(row=row, column=1).value != None:
            module = 'M' + str(int(sheet.cell(row=row, column=1).value))
            if module in modulesInfo.keys():
                # allow for the possibility that the HDI info is added later
                if modulesInfo[module]['HDI'] == None and sheet.cell(row=row, column=8).value != None:
                    print 'Adding HDI info for module', module
                    modulesInfo[module]['HDI'] = str(sheet.cell(row=row, column=8).value)
                    newHDI.append(module)
            else:
                newModules.append(module)
                modulesInfo[module] = ModuleInfo().attributes
                modulesInfo[module]['Sensor'] = str(sheet.cell(row=row, column=6).value) + '-' + str(int(sheet.cell(row=row, column=7).value))
                if sheet.cell(row=row, column=8).value != None:
                    modulesInfo[module]['HDI'] = str(sheet.cell(row=row, column=8).value)
        if sheet.cell(row=row, column=2).value != None:
            chipNo = str(int(sheet.cell(row=row, column=4).value))
            chip = 'Chip' + chipNo
            if modulesInfo[module][chip] == None:
                print 'Adding', chip, 'info for module', module
                modulesInfo[module]['Chip' + chipNo] = str(sheet.cell(row=row, column=2).value) + '-' + str(sheet.cell(row=row, column=3).value)
            else:
                if sheet.cell(row=row, column=1).value != None:
                    print 'Assembly info for module', module, 'already present. Skipping'
        else:
            break

    #print modulesInfo

    if len(newModules) == 0 and len(newHDI) == 0:
        print 'No new module assembly info found. Nothing to write'
    else:
        if len(newModules) > 0:
            newModules.sort()
            print 'New assembly info added for module(s)', ', '.join(newModules)
        if len(newHDI) > 0:
            newHDI.sort()
            print 'New HDI info added for module(s)', ', '.join(newHDI)
        # check if the output file already exists
        if os.path.exists(options.outDB):
            question = "Output file '" + options.outDB + "' already exists. Overwrite?"
            if query_yes_no(question, default="no"):
                write_db(modulesInfo, options.outDB)
        else:
            write_db(modulesInfo, options.outDB)


if __name__ == '__main__':
  main()
