import os
import sys
import pickle
import hashlib
from optparse import OptionParser
from openpyxl import load_workbook


def main():
    # usage description
    usage = "Usage: python %prog [options] \nExample: python %prog -m M1600"

    # input parameters
    parser = OptionParser(usage=usage)

    parser.add_option("-m", "--modules", dest="modules",
                      help="Comma-separated list of modules",
                      metavar="MODULES")

    parser.add_option("-l", "--locations", dest="locations",
                      help="Comma-separated list of module locations",
                      metavar="LOCATIONS")

    parser.add_option("-o", "--output", dest="output",
                      help="Output file to which the module locations will be stored",
                      metavar="OUTPUT")

    parser.add_option("--xlsURL", dest="xlsURL",
                      help="URL of an Excel spreadsheet containing module location information (This parameter is optional and set to 'https://docs.google.com/spreadsheets/d/1PTlNGoXFR4mV685KnVLmydQnlJ2bUdSRyfKQmNT5pmE/export?format=xlsx' by default)",
                      default='https://docs.google.com/spreadsheets/d/1PTlNGoXFR4mV685KnVLmydQnlJ2bUdSRyfKQmNT5pmE/export?format=xlsx',
                      metavar="XLSURL")

    parser.add_option("--sheet", dest="sheet",
                      help="Data sheet from the Excel spreadsheet to be used (This parameter is optional and set to 'POS' by default)",
                      default='POS',
                      metavar="SHEET")

    parser.add_option("-f", "--force", dest="force", action='store_true',
                      help="Force rebuilding of the local DB file",
                      default=False)

    parser.add_option("-v", "--verbose", dest="verbose", action='store_true',
                      help="Verbose output",
                      default=False)

    (options, args) = parser.parse_args()

    # make sure all necessary input parameters are provided
    if not (options.modules or options.locations or options.output):
        print 'Either module or module location need to be specified'
        print ''
        parser.print_help()
        sys.exit(1)

    hash_object = hashlib.md5(options.xlsURL.encode())
    #print(hash_object.hexdigest())
    local_cache_name = hash_object.hexdigest()

    local_xls = local_cache_name + '.xlsx'
    local_db  = local_cache_name + '.pkl'

    module_location = {}
    location_module = {}

    if os.path.exists(local_db) and not options.force:
        if options.verbose:
            print('Loading module location DB file...')
        with open(local_db, 'rb') as fpkl:
            (module_location, location_module) = pickle.load(fpkl)
        if options.verbose:
            print("DB file '%s' loaded"%local_db)
    else:
        if not os.path.exists(local_xls) or options.force:
            print('Downloading module location spreadsheet...')
            cmd = 'wget -O %s %s' % (local_xls, options.xlsURL)
            os.system(cmd)
            print("Spreadsheet saved to '%s'"%local_xls)

        # load the Excel spreadsheet
        wb = load_workbook(local_xls, data_only=True)

        # get sheet names
        #print(wb.get_sheet_names())

        # get a sheet by name
        sheet = wb.get_sheet_by_name(options.sheet)

        rowMin = 19
        rowMax = 30
        columnMin = 16
        columnMax = 23

        for row in range(rowMin, rowMax+1):
            for column in range(columnMin, columnMax+1):
                cell = str(sheet.cell(row=row, column=column).value).strip()
                module   = cell[0:5]
                location = cell[6:]
                if module in module_location.keys():
                    print('')
                    print('WARNING: Module %s already present' % module)
                else:
                    module_location[module] = location
                if location in location_module.keys():
                    print('')
                    print('WARNING: Location %s already occupied' % location)
                else:
                    location_module[location]  = module

        # dump module location data into a pickle file
        if options.verbose:
            print('Saving module location data into a DB file...')
        dump = (module_location, location_module)
        with open(local_db, 'wb') as fpkl:
            pickle.dump(dump, fpkl)
        if options.verbose:
            print("DB file '%s' saved"%local_db)

    print('')
    print('Total number of modules: %i' % len(module_location.keys()))
    if len(module_location.keys()) != 96:
        print('WARNING: Total number of modules different from 96')

    if options.modules:
        print('')
        modules = options.modules.split(',')
        for m in sorted( modules ):
            if m in module_location.keys():
                print('Module %s in located at %s' % (m, module_location[m]) )
            else:
                print('WARNING: Unknown module %s' % m)
        print('')

    if options.locations:
        locations = options.locations.split(',')
        for l in sorted( locations ):
            if l in location_module.keys():
                print('Location %s corresponds to module %s' % (l, location_module[l]) )
            else:
                print('WARNING: Unknown location %s' % l)
        print('')

    if options.output:
        modules = []
        if options.modules:
            modules = options.modules.split(',')
            knownModules = False
            for m in modules:
                if m in module_location.keys():
                    knownModules = True
                    break
            if not knownModules:
                print('WARNING: No known modules specified. No output file written')
                print('')
                return
        output_file = open(options.output,'w')
        for m in sorted( module_location.keys() ):
            if options.modules and m not in modules:
                continue
            output_file.write(m + ' ' + ' '.join(module_location[m].split('_')) + '\n')
        output_file.close()
        if not options.modules:
            print('')
        print("Module locations written to '%s'" % options.output)
        print('')

if __name__ == '__main__':
    main()
