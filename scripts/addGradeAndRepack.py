import os
import sys
import shlex, subprocess
import tarfile
from optparse import OptionParser


def exec_cmd(cmd):
    print cmd
    os.system(cmd)


def main():
    # usage description
    usage = "Usage: python %prog -i InputDir -o OutputDir"

    # input parameters
    parser = OptionParser(usage=usage)

    parser.add_option("-i", "--input_dir", dest="input_dir",
                      help="Inut directory",
                      metavar="INPUT")

    parser.add_option("-o", "--output_dir", dest="output_dir",
                      help="Output directory",
                      metavar="OUTPUT")

    parser.add_option("-m", "--modules", dest="modules",
                      help="Comma-separated list of modules or module ranges (just number without the leading M)",
                      metavar="MODULES")

    (options, args) = parser.parse_args()

    # make sure all necessary input parameters are provided
    if not (options.input_dir and options.output_dir):
        print 'Mandatory input arguments missing'
        print ''
        parser.print_help()
        sys.exit(1)

    input_dir = options.input_dir
    if not input_dir.startswith('/'):
        input_dir = os.path.join( os.path.abspath('.'), input_dir )

    output_dir = options.output_dir
    if not output_dir.startswith('/'):
        output_dir = os.path.join( os.path.abspath('.'), output_dir )

    os.chdir(input_dir)

    cmd = 'find ./ -type f -name grade.txt'
    print cmd
    output =  subprocess.check_output(shlex.split(cmd))
    print output

    folders = [f.split('/')[1] for f in output.split('\n') if f != '']

    folders = list(set(folders))
    folders.sort()

    os.chdir(output_dir)

    for f in folders:
        fullpath = os.path.join(input_dir,f)

        module = f.split('_')[0][1:]
        skip = True

        if options.modules:
            modules = options.modules.split(',')
            for m in modules:
                if '-' in m:
                    m_range = m.split('-')
                    m_low, m_high = [m_range[i] for i in (0, 1)]
                    if m_high != '':
                        if int(module) >= int(m_low) and int(module) <= int(m_high):
                            skip = False
                            break
                    else:
                        if int(module) >= int(m_low):
                            skip = False
                            break
                else:
                    if module == m:
                        skip = False
                        break
        else:
            skip = False

        if skip:
            continue

        print ''
        print f

        cmd = 'cp %s.tar %s' % (fullpath, output_dir)
        exec_cmd(cmd)

        cmd = 'tar xfz %s.tar' % f
        exec_cmd(cmd)

        cmd = 'rm %s.tar' % f
        exec_cmd(cmd)

        cmd = 'rsync -avPSh  --prune-empty-dirs --include "*/"  --include="grade.txt" --exclude="*" %s/ %s/' % (fullpath, os.path.join(output_dir, f))
        exec_cmd(cmd)

        print 'Making tar file'
        tar = tarfile.open( '%s.tar' % f, "w:gz")
        tar.add(f)
        tar.close()

        cmd = 'touch -r %s.tar %s.tar' % (fullpath, os.path.join(output_dir, f))
        exec_cmd(cmd)

        cmd = 'rm -rf %s' % f
        exec_cmd(cmd)


if __name__ == '__main__':
    main()
