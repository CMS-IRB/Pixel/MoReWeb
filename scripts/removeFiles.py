import os
import sys
from optparse import OptionParser


def exec_cmd(cmd):
    print cmd
    os.system(cmd)


def main():
    # usage description
    usage = "Usage: python %prog -l LIST"

    # input parameters
    parser = OptionParser(usage=usage)

    parser.add_option("-l", "--list", dest="list",
                      help="File containing a list of files to be deleted (This parameter is mandatory)",
                      metavar="LIST")

    (options, args) = parser.parse_args()

    # make sure all necessary input parameters are provided
    if not options.list:
        print 'Mandatory input argument missing'
        print ''
        parser.print_help()
        sys.exit(1)

    files = file(options.list).readlines()

    for line in files:
        f = line.strip('\n')
        if not os.path.exists(f):
            print f, 'does not exist'
            continue
        if not os.path.isfile(f):
            print f, 'is not a file'
            continue

        cmd = 'rm -v "%s"' % f
        exec_cmd(cmd)


if __name__ == '__main__':
    main()
