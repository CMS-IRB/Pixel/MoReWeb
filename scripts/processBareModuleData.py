import os
import sys
from optparse import OptionParser
from makeProductionDB import query_yes_no


def exec_cmd(cmd):
    print cmd
    os.system(cmd)


def main():
    # usage description
    usage = "Usage: python %prog -i InputDir -o OutputDir"

    # input parameters
    parser = OptionParser(usage=usage)

    parser.add_option("-i", "--input_dir", dest="input_dir",
                      help="Inut directory",
                      metavar="INPUT")

    parser.add_option("-o", "--output_dir", dest="output_dir",
                      help="Output directory",
                      metavar="OUTPUT")

    parser.add_option("--overwrite", dest="overwrite", action='store_true',
                      help="Overwrite destination files (This parameter is optional)",
                      default=False)


    (options, args) = parser.parse_args()

    # make sure all necessary input parameters are provided
    if not (options.input_dir and options.output_dir):
        print 'Mandatory input arguments missing'
        print ''
        parser.print_help()
        sys.exit(1)

    if options.overwrite:
        question = "WARNING: You selected to overwrite destination files if they already exist. Really overwrite?"
        if not query_yes_no(question, default="no"):
            options.overwrite = False

    content = os.listdir(options.input_dir)
    content.sort()
    for f in content:
        fullpath = os.path.join(options.input_dir,f)

        if not os.path.isfile(fullpath): continue

        if '.old.' in f: continue

        extensions = ('.bmp', '.log', '.png', '.txt')
        if not f.lower().endswith(extensions): continue

        dir_name_parts = os.path.splitext(f)[0].split('-')

        dir_name = 'B' + dir_name_parts[0] + '-' + dir_name_parts[1] + '-' +dir_name_parts[2][0]

        output_dir = os.path.join(options.output_dir, dir_name)

        if not os.path.exists(output_dir):
            os.mkdir(output_dir)
        #else:
            #print "Output directory '" + output_dir +"' already exists"

        if f.lower().endswith('.png') or f.lower().endswith('.txt'):
            newpath = os.path.join(output_dir, 'B' + f)
            cmd = 'cp ' + fullpath + ' ' + newpath
            # check if the output file already exists
            if os.path.exists(newpath):
                print "Output file '" + newpath + "' already exists"
                if options.overwrite:
                    print "Overwriting"
                    exec_cmd(cmd)
            else:
                exec_cmd(cmd)

        if f.lower().endswith('.log'):
            newpath = os.path.join(output_dir, 'B' + os.path.splitext(f)[0] + '_log.txt')
            cmd = 'cp ' + fullpath + ' ' + newpath
            # check if the output file already exists
            if os.path.exists(newpath):
                print "Output file '" + newpath + "' already exists"
                if options.overwrite:
                    print "Overwriting"
                    exec_cmd(cmd)
            else:
                exec_cmd(cmd)

        if f.lower().endswith('.bmp'):
            newpath = os.path.join(output_dir, 'B' + os.path.splitext(f)[0] + '.png')
            # check if the output file already exists
            cmd = 'convert ' + fullpath + ' ' + newpath
            if os.path.exists(newpath):
                print "Output file '" + newpath + "' already exists"
                if options.overwrite:
                    print "Overwriting"
                    exec_cmd(cmd)
            else:
                exec_cmd(cmd)


if __name__ == '__main__':
    main()
