import os
import sys
from optparse import OptionParser


def main():
    # usage description
    usage = "Usage: python %prog [options] \nExample: python prepareDataForPOS.py -i module_locations.txt -p /home/l_tester/L1_DATA/ -m /home/l_tester/L1_DATA/WebOutput/MoReWeb/FinalResults/REV001/R001/ -l /home/l_tester/L1_DATA/POS_files/Folder_links/"

    # input parameters
    parser = OptionParser(usage=usage)

    parser.add_option("-i", "--input", dest="input",
                      help="Input file with module locations",
                      metavar="INPUT")

    parser.add_option("-p", "--pxar", dest="pxar",
                      help="Path to pXar data",
                      metavar="PXAR")

    parser.add_option("-m", "--moreweb", dest="moreweb",
                      help="Path to MoReWeb data",
                      metavar="MOREWEB")

    parser.add_option("-l", "--links", dest="links",
                      help="Path to folder containing folder links",
                      metavar="LINKS")

    parser.add_option("-v", "--verbose", dest="verbose", action='store_true',
                      help="Verbose output",
                      default=False)

    (options, args) = parser.parse_args()

    # make sure all necessary input parameters are provided
    if not (options.input and options.pxar and options.moreweb and options.links):
        print 'Missing mandatory input arguments'
        print ''
        parser.print_help()
        sys.exit(1)

    modules = []
    modules_pxar_folders = {}

    module_locations = open(options.input).read().splitlines()
    for l in module_locations:
        m = l.split()[0]
        modules.append(m)
        modules_pxar_folders[m] = []

    modules_tuple = tuple(modules)

    for l in sorted( os.listdir(options.pxar) ):
        full_path = os.path.join(options.pxar, l)
        if not os.path.isdir(full_path):
            continue
        if 'FullQualification' not in l:
            continue
        if not l.startswith(modules_tuple):
            continue
        m = l.split('_')[0]
        modules_pxar_folders[m].append(l)

    modules_pxar_latest_folder = {}
    for m in modules_pxar_folders.keys():
        if len(modules_pxar_folders[m]) == 0:
            print('ERROR: No pXar folders found for module %s. Aborting' %m)
            sys.exit(2)
        modules_pxar_latest_folder[m] = modules_pxar_folders[m][-1]

    for m in modules:
        moreweb_path = os.path.join(options.moreweb, modules_pxar_latest_folder[m], 'QualificationGroup/ModuleFulltest_m20_1/Chips/ChipCHIP/ReadbackCal/KeyValueDictPairs.json')
        pxar_path    = os.path.join(options.pxar,    modules_pxar_latest_folder[m], '000_Fulltest_m20/ReadbackCHIP.json')

        # copy readback calibration files
        cmd_tmpl = 'cp %s %s' % (moreweb_path, pxar_path)

        for i in range(0,16):
            cmd = cmd_tmpl.replace('CHIP',str(i))
            if options.verbose:
                print(cmd)
            os.system(cmd)

        # create folder links
        pxar_path = os.path.join(options.pxar,  modules_pxar_latest_folder[m])
        link_path = os.path.join(options.links, modules_pxar_latest_folder[m])

        cmd = 'ln -sf %s %s' % (pxar_path, link_path)
        if options.verbose:
            print(cmd)
        os.system(cmd)


if __name__ == '__main__':
    main()
