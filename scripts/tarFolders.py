import os
import sys
import tarfile
from optparse import OptionParser


def main():
    # usage description
    usage = "Usage: python %prog -d DIR1,DIR2"

    # input parameters
    parser = OptionParser(usage=usage)

    parser.add_option("-d", "--dirs", dest="dirs",
                      help="Comma-separated list of sub-folders to tar",
                      metavar="DIR1,DIR2")

    parser.add_option("--all", dest="all", action='store_true',
                      help="Tar all sub-folders",
                      default=False)

    (options, args) = parser.parse_args()

    # make sure all necessary input parameters are provided
    if not (options.dirs or options.all):
        print 'Mandatory input arguments missing'
        print ''
        parser.print_help()
        sys.exit(1)

    dir_list = []

    if options.all:
        content = os.listdir('./')
        content.sort()

        for d in content:
            if os.path.isfile(d): continue
            dir_list.append(d)
    else:
        dir_list = options.dirs.split(',')

    for d in dir_list:
        print 'Tarring', d + '...'

        tar = tarfile.open(d + '.tar', "w:gz")
        tar.add(d)
        tar.close()

        print 'Done'


if __name__ == '__main__':
    main()
