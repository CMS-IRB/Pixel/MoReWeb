import os
import sys
import pickle
import ConfigParser
import AbstractClasses


class TestResult(AbstractClasses.GeneralTestResult.GeneralTestResult):
    def CustomInit(self):
        self.NameSingle = 'WaferInfo'
        self.Name = 'CMSPixel_QualificationGroup_Fulltest_Chips_Chip_%s_TestResult'%self.NameSingle
        self.Attributes['TestedObjectType'] = 'CMSPixel_QualificationGroup_Fulltest_ROC'

    def PopulateResultData(self):

        chipNo   = self.ParentObject.Attributes['ChipNo']
        moduleID = self.ParentObject.ParentObject.ParentObject.Attributes['ModuleID']

        Configuration = ConfigParser.ConfigParser()
        Configuration.read(['Configuration/Paths.cfg'])

        try:
            ModuleInfoDBPath = Configuration.get('Paths', 'ModuleInfoDBPath')
        except:
            print "Error: Could not find the ModuleInfoDB path. Aborting!"
            sys.exit(-1)

        modulesInfo = {}

        if not os.path.exists(ModuleInfoDBPath):
            print "Error: ModuleInfoDB file missing. Aborting!"
            sys.exit(-1)
        else:
            with open(ModuleInfoDBPath, 'rb') as fpkl:
                modulesInfo = pickle.load(fpkl)

        linkHTMLTemplate = self.TestResultEnvironmentObject.HtmlParser.getSubpart(
            self.TestResultEnvironmentObject.OverviewHTMLTemplate,
            '###LINK###'
        )

        chip = modulesInfo[moduleID]['Chip' + str(chipNo)]

        linkHTML = self.TestResultEnvironmentObject.HtmlParser.substituteMarkerArray(
            linkHTMLTemplate,
            {
                '###LABEL###': chip,
                '###URL###': '../../../../../../../../../ROC_Wafers/' + chip.split('-')[0]
            }
        )

        self.ResultData['Table'] = {'BODY':[['Wafer-ROC:', linkHTML]]}
