import os
import sys
import pickle
import ConfigParser
import AbstractClasses


class TestResult(AbstractClasses.GeneralTestResult.GeneralTestResult):
    def CustomInit(self):
        self.NameSingle = 'BareModuleInfo'
        self.Name = 'CMSPixel_QualificationGroup_%s_TestResult'%self.NameSingle
        self.Title = str(self.Attributes['ModuleID']) + ' ' + self.Attributes['StorageKey']
        self.Attributes['TestedObjectType'] = 'CMSPixel_Module'

    def PopulateResultData(self):

        moduleID = self.Attributes['ModuleID']

        Configuration = ConfigParser.ConfigParser()
        Configuration.read(['Configuration/Paths.cfg'])

        try:
            ModuleInfoDBPath = Configuration.get('Paths', 'ModuleInfoDBPath')
        except:
            print "Error: Could not find the ModuleInfoDB path. Aborting!"
            sys.exit(-1)

        modulesInfo = {}

        if not os.path.exists(ModuleInfoDBPath):
            print "Error: ModuleInfoDB file missing. Aborting!"
            sys.exit(-1)
        else:
            with open(ModuleInfoDBPath, 'rb') as fpkl:
                modulesInfo = pickle.load(fpkl)

        linkHTMLTemplate = self.TestResultEnvironmentObject.HtmlParser.getSubpart(
            self.TestResultEnvironmentObject.OverviewHTMLTemplate,
            '###LINK###'
        )

        sensor = modulesInfo[moduleID]['Sensor']
        bareModuleID = 'B' + sensor

        linkHTML = self.TestResultEnvironmentObject.HtmlParser.substituteMarkerArray(
            linkHTMLTemplate,
            {
                '###LABEL###': bareModuleID,
                '###URL###': '../../../../../../BareModules/' + bareModuleID
            }
        )

        self.ResultData['Table'] = {'BODY':[['BareModule:', linkHTML]]}
